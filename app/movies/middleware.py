from django.http import HttpResponseForbidden


class CheckUserGroupMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)

        if not request.user.is_authenticated:
            return self.get_response(request)

        user_groups = set(request.user.groups.values_list("name", flat=True))

        if "Administrator" in user_groups:
            return response
        elif "PM" in user_groups and request.method in ["GET", "PUT"]:
            return response
        elif "Default" in user_groups and request.method in ["GET"]:
            return response
        else:
            return HttpResponseForbidden("You don't have permission to access this resource.")
