import pytest
import json
import base64
from django.contrib.auth.models import Group, User
from movies.models import Movies, CustomUser


@pytest.mark.django_db
def test_get_all_movies(client, add_movie):
    new_group, create = Group.objects.get_or_create(name="Administrator")
    user = CustomUser.objects.create_user(username="username_1", password="password_1")
    user.groups.add(new_group)

    # Generate the base64 encoded credentials
    credentials = base64.b64encode(b"username_1:password_1").decode("utf-8")

    movie_one = add_movie(title="The Big Lebowski", genre="comedy", year="1998")
    movie_two = add_movie("No Country for Old Men", "thriller", "2007")
    resp = client.get("/api/movies/",
                      HTTP_AUTHORIZATION=f"Basic {credentials}",)  # Add the Authorization header
    assert resp.status_code == 200
    assert resp.data[0]["title"] == movie_one.title
    assert resp.data[1]["title"] == movie_two.title


@pytest.mark.django_db
def test_add_movie(client):
    movies = Movies.objects.all()
    assert len(movies) == 0

    new_group, create = Group.objects.get_or_create(name="Administrator")
    user = CustomUser.objects.create_user(username="username_1", password="password_1")
    user.groups.add(new_group)

    # Generate the base64 encoded credentials
    credentials = base64.b64encode(b"username_1:password_1").decode("utf-8")

    resp = client.post(
        "/api/movies/",
        json.dumps({       # Use json.dumps() to convert the dictionary to a JSON string
            "title": "The Big Lebowski",
            "genre": "comedy",
            "year": "1998",
        }),
        content_type="application/json",
        HTTP_AUTHORIZATION=f"Basic {credentials}",  # Add the Authorization header
    )
    assert resp.status_code == 201
    assert resp.data["title"] == "The Big Lebowski"

    movies = Movies.objects.all()
    assert len(movies) == 1


@pytest.mark.django_db
def test_add_movie_invalid_json(client):
    movies = Movies.objects.all()
    assert len(movies) == 0

    new_group, create = Group.objects.get_or_create(name="Administrator")
    user = CustomUser.objects.create_user(username="username_1", password="password_1")
    user.groups.add(new_group)

    credentials = base64.b64encode(b"username_1:password_1").decode("utf-8")

    resp = client.post("/api/movies/",
                       {},
                       content_type="application/json",
                       HTTP_AUTHORIZATION=f"Basic {credentials}",)
    assert resp.status_code == 400

    movies = Movies.objects.all()
    assert len(movies) == 0


@pytest.mark.django_db
def test_add_movie_invalid_json_keys(client):
    movies = Movies.objects.all()
    assert len(movies) == 0

    new_group, create = Group.objects.get_or_create(name="Administrator")
    user = CustomUser.objects.create_user(username="username_1", password="password_1")
    user.groups.add(new_group)

    credentials = base64.b64encode(b"username_1:password_1").decode("utf-8")

    resp = client.post(
        "/api/movies/",
        {
            "title": "The Big Lebowski",
            "genre": "comedy",
        },
        content_type="application/json",
        HTTP_AUTHORIZATION=f"Basic {credentials}",
    )
    assert resp.status_code == 400

    movies = Movies.objects.all()
    assert len(movies) == 0


@pytest.mark.django_db
def test_get_single_movie(client, add_movie):
    movie = add_movie(title="The Big Lebowski", genre="comedy", year="1998")

    new_group, create = Group.objects.get_or_create(name="Administrator")
    user = CustomUser.objects.create_user(username="username_1", password="password_1")
    user.groups.add(new_group)

    credentials = base64.b64encode(b"username_1:password_1").decode("utf-8")

    resp = client.get(f"/api/movies/{movie.id}/",
                      HTTP_AUTHORIZATION=f"Basic {credentials}",)
    assert resp.status_code == 200
    assert resp.data["title"] == "The Big Lebowski"

@pytest.mark.django_db
def test_get_single_movie_incorrect_id(client):
    new_group, create = Group.objects.get_or_create(name="Administrator")
    user = CustomUser.objects.create_user(username="username_1", password="password_1")
    user.groups.add(new_group)

    credentials = base64.b64encode(b"username_1:password_1").decode("utf-8")
    resp = client.get("/api/movies/foo/",
                      HTTP_AUTHORIZATION=f"Basic {credentials}",)
    assert resp.status_code == 404


@pytest.mark.django_db
def test_remove_movie(client, add_movie):
    movie = add_movie(title="The Big Lebowski", genre="comedy", year="1998")

    new_group, create = Group.objects.get_or_create(name="Administrator")
    user = CustomUser.objects.create_user(username="username_1", password="password_1")
    user.groups.add(new_group)

    credentials = base64.b64encode(b"username_1:password_1").decode("utf-8")

    resp = client.get(f"/api/movies/{movie.id}/",
                      HTTP_AUTHORIZATION=f"Basic {credentials}",)

    assert resp.status_code == 200
    assert resp.data["title"] == "The Big Lebowski"

    new_group, create = Group.objects.get_or_create(name="Administrator")
    user = CustomUser.objects.create_user(username="username_2", password="password_1")
    user.groups.add(new_group)

    credentials = base64.b64encode(b"username_1:password_1").decode("utf-8")

    resp_two = client.delete(f"/api/movies/{movie.id}/",
                      HTTP_AUTHORIZATION=f"Basic {credentials}",)
    assert resp_two.status_code == 204

    new_group, create = Group.objects.get_or_create(name="Administrator")
    user = CustomUser.objects.create_user(username="username_3", password="password_1")
    user.groups.add(new_group)

    credentials = base64.b64encode(b"username_1:password_1").decode("utf-8")

    resp_three = client.get("/api/movies/",
                      HTTP_AUTHORIZATION=f"Basic {credentials}",)
    assert resp_three.status_code == 200
    assert len(resp_three.data) == 0


@pytest.mark.django_db
def test_remove_movie_incorrect_id(client):
    new_group, create = Group.objects.get_or_create(name="Administrator")
    user = CustomUser.objects.create_user(username="username_1", password="password_1")
    user.groups.add(new_group)

    credentials = base64.b64encode(b"username_1:password_1").decode("utf-8")

    resp = client.delete(f"/api/movies/99/",
                         HTTP_AUTHORIZATION=f"Basic {credentials}",)
    assert resp.status_code == 404


@pytest.mark.django_db
def test_update_movie(client, add_movie):
    movie = add_movie(title="The Big Lebowski", genre="comedy", year="1998")

    new_group, create = Group.objects.get_or_create(name="Administrator")
    user = CustomUser.objects.create_user(username="username_1", password="password_1")
    user.groups.add(new_group)

    credentials = base64.b64encode(b"username_1:password_1").decode("utf-8")

    resp = client.put(
        f"/api/movies/{movie.id}/",
        {"title": "The Big Lebowski", "genre": "comedy", "year": "1997"},
        content_type="application/json",
        HTTP_AUTHORIZATION=f"Basic {credentials}",
    )
    assert resp.status_code == 200
    assert resp.data["title"] == "The Big Lebowski"
    assert resp.data["year"] == "1997"

    new_group, create = Group.objects.get_or_create(name="Administrator")
    user = CustomUser.objects.create_user(username="username_2", password="password_1")
    user.groups.add(new_group)

    credentials = base64.b64encode(b"username_1:password_1").decode("utf-8")

    resp_two = client.get(f"/api/movies/{movie.id}/",
        HTTP_AUTHORIZATION=f"Basic {credentials}",)
    assert resp_two.status_code == 200
    assert resp_two.data["title"] == "The Big Lebowski"
    assert resp.data["year"] == "1997"


@pytest.mark.django_db
def test_update_movie_incorrect_id(client):
    new_group, create = Group.objects.get_or_create(name="Administrator")
    user = CustomUser.objects.create_user(username="username_1", password="password_1")
    user.groups.add(new_group)

    credentials = base64.b64encode(b"username_1:password_1").decode("utf-8")

    resp = client.put(f"/api/movies/99/",
                      HTTP_AUTHORIZATION=f"Basic {credentials}",)
    assert resp.status_code == 404


@pytest.mark.django_db
@pytest.mark.parametrize("add_movie, payload, status_code", [
    ["add_movie", {}, 400],
    ["add_movie",{"title": "The Big Lebowski", "genre": "comedy"}, 400],
], indirect=["add_movie"])
def test_update_movie_invalid_json(client, add_movie, payload, status_code):
    movie = add_movie(title="The Big Lebowski", genre="comedy", year="1998")

    new_group, create = Group.objects.get_or_create(name="Administrator")
    user = CustomUser.objects.create_user(username="username_1", password="password_1")
    user.groups.add(new_group)

    credentials = base64.b64encode(b"username_1:password_1").decode("utf-8")

    resp = client.put(
        f"/api/movies/{movie.id}/",
        payload,
        content_type="application/json",
        HTTP_AUTHORIZATION=f"Basic {credentials}",
    )
    assert resp.status_code == status_code