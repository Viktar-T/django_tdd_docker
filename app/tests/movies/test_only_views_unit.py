# all the same tests as in test_views.py, but we use monkeypatch to replace
# the MovieSerializer serializer, MovieDetail view, and Movie model.
#
# we use RequestFactory to skip middleware
# we use force_authenticate() to skip authentication

import pytest
import json

from django.http import Http404
from django.test import RequestFactory
from rest_framework.parsers import JSONParser
from rest_framework.test import force_authenticate
from movies.models import Movies, CustomUser
from movies.views import MovieList, MovieDetail  # Import your view class
from movies.serializers import MovieSerializer

@pytest.mark.django_db
def test_add_movie(rf, monkeypatch):  # Replace 'client' with 'rf'
    payload = {"title": "The Big Lebowski", "genre": "comedy", "year": "1998"}

    def mock_create(self, payload):
        return "The Big Lebowski"

    monkeypatch.setattr(MovieSerializer, "create", mock_create)
    monkeypatch.setattr(MovieSerializer, "data", payload)

    # Create a CustomUser instance with the desired group
    user = CustomUser(username="username_1")
    user.set_password("password_1")
    user.save()

    # Create a !request! using RequestFactory
    # In the context of the test example I provided earlier, rf is used as a fixture for the
    # RequestFactory. This fixture is provided by the pytest-django plugin, which integrates
    # Django with the pytest testing framework.
    request = rf.post(
        "/api/movies/",
        json.dumps(payload),
        content_type="application/json",
    )

    # Authenticate the user
    force_authenticate(request, user=user)

    # Call the view with the request
    # In Django, class-based views (CBVs) provide a way to structure your views as classes
    # with multiple methods representing different HTTP verbs like GET, POST, PUT, and DELETE.
    # To utilize these class-based views in your URLs or tests, you need to convert them into
    # a callable function that can be called with a request object.
    #
    # The as_view() method is a class method provided by Django's class-based views.
    # It is used to create a callable function from the class-based view, allowing you to
    # call the view with a request object. The as_view() method returns a function that,
    # when called with a request, routes the request to the appropriate method
    # (e.g., get(), post(), put(), delete()) based on the request's HTTP verb.
    view = MovieList.as_view()  # Call the as_view() method on your view class
    response = view(request)  # Pass the request object to the callable returned by as_view()

    # Test the response
    assert response.status_code == 201
    assert response.data["title"] == "The Big Lebowski"

@pytest.mark.django_db
def test_add_movie_invalid_json(rf):
    user = CustomUser(username="username_1")
    user.set_password("password_1")
    user.save()

    request = rf.post(
        "/api/movies/",
        json.dumps({}),
        content_type="application/json",
    )

    force_authenticate(request, user=user)

    view = MovieList.as_view()
    response = view(request)

    assert response.status_code == 400

@pytest.mark.django_db
def test_add_movie_invalid_json_keys(rf):
    user = CustomUser(username="username_1")
    user.set_password("password_1")
    user.save()

    payload = {"title": "The Big Lebowski", "genre": "comedy"}
    request = rf.post(
        "/api/movies/",
        json.dumps(payload),
        content_type="application/json",
    )

    force_authenticate(request, user=user)

    view = MovieList.as_view()
    response = view(request)

    assert response.status_code == 400

@pytest.mark.django_db
def test_get_single_movie(rf, monkeypatch):
    payload = {"title": "The Big Lebowski", "genre": "comedy", "year": "1998"}

    def mock_get_object(self, pk):
        return 1

    monkeypatch.setattr(MovieDetail, "get_object", mock_get_object)
    monkeypatch.setattr(MovieSerializer, "data", payload)

    user = CustomUser(username="username_1")
    user.set_password("password_1")
    user.save()

    # Change the request method to 'get' and update the URL to include the movie ID
    request = rf.get(
        "/api/movies/1/",
        content_type="application/json",
    )

    force_authenticate(request, user=user)
    view = MovieDetail.as_view()  # Call the as_view() method on your view class
    resp = view(request, pk='1')

    assert resp.status_code == 200
    assert resp.data["title"] == "The Big Lebowski"

@pytest.mark.django_db
def test_get_single_movie_incorrect_id(client):
    resp = client.get("/api/movies/foo/")
    assert resp.status_code == 404

@pytest.mark.django_db
def test_get_all_movies(rf, monkeypatch):
    user = CustomUser(username="username_1")
    user.set_password("password_1")
    user.save()

    request = rf.get(
        "/api/movies/",
        content_type="application/json",
    )

    payload = [
        {"title": "The Big Lebowski", "genre": "comedy", "year": "1998"},
        {"title": "No Country for Old Men", "genre": "thriller", "year": "2007"},
    ]

    def mock_get_all_movies():
        return payload

    monkeypatch.setattr(Movies.objects, "all", mock_get_all_movies)
    monkeypatch.setattr(MovieSerializer, "data", payload)

    force_authenticate(request, user=user)

    view = MovieList.as_view()
    resp = view(request)

    assert resp.status_code == 200
    assert resp.data[0]["title"] == payload[0]["title"]
    assert resp.data[1]["title"] == payload[1]["title"]

@pytest.mark.django_db
def test_remove_movie(rf, monkeypatch):
    def mock_get_object(self, pk):
        class Movie:
            def delete(self):
                pass

        return Movie()

    monkeypatch.setattr(MovieDetail, "get_object", mock_get_object)

    user = CustomUser(username="username_1")
    user.set_password("password_1")
    user.save()

    request = rf.delete(
        "/api/movies/1/",
        content_type="application/json",
    )

    force_authenticate(request, user=user)

    view = MovieDetail.as_view()
    response = view(request, pk='1')
    assert response.status_code == 204


@pytest.mark.django_db
def test_remove_movie_incorrect_id(rf, monkeypatch):
    user = CustomUser(username="username_1")
    user.set_password("password_1")
    user.save()

    request = rf.delete(
        "/api/movies/99/",
        content_type="application/json",
    )

    force_authenticate(request, user=user)

    def mock_get_object(self, pk):
        raise Http404

    monkeypatch.setattr(MovieDetail, "get_object", mock_get_object)

    view = MovieDetail.as_view()
    resp = view(request, pk='99')
    assert resp.status_code == 404


@pytest.mark.django_db
def test_update_movie(rf, monkeypatch):  # Replace 'client' with 'rf'
    payload = {"title": "The Big Lebowski", "genre": "comedy", "year": "1997"}

    def mock_get_object(self, pk):
        return 1

    def mock_update_object(self, movie_object, data):
        return payload

    monkeypatch.setattr(MovieDetail, "get_object", mock_get_object)
    monkeypatch.setattr(MovieSerializer, "update", mock_update_object)

    user = CustomUser(username="username_1")
    user.set_password("password_1")
    user.save()

    request = rf.put(
        "/api/movies/1/",
        json.dumps(payload),
        content_type="application/json",
    )

    force_authenticate(request, user=user)
    view = MovieDetail.as_view()
    response = view(request, pk='1')

    assert response.status_code == 200
    assert response.data["title"] == payload["title"]
    assert response.data["year"] == payload["year"]


@pytest.mark.django_db
def test_update_movie_incorrect_id(rf, monkeypatch):
    user = CustomUser(username="username_1")
    user.set_password("password_1")
    user.save()

    request = rf.put(
        "/api/movies/99/",
        content_type="application/json",
    )

    force_authenticate(request, user=user)

    def mock_get_object(self, pk):
        raise Http404

    monkeypatch.setattr(MovieDetail, "get_object", mock_get_object)

    view = MovieDetail.as_view()
    resp = view(request, pk='99')
    assert resp.status_code == 404


@pytest.mark.django_db
@pytest.mark.parametrize(
    "payload, status_code",
    [[{}, 400], [{"title": "The Big Lebowski", "genre": "comedy"}, 400]],
)
def test_update_movie_invalid_json(rf, monkeypatch, payload, status_code):
    user = CustomUser(username="username_1")
    user.set_password("password_1")
    user.save()

    request = rf.put(
        "/api/movies/1/",
        content_type="application/json",
    )

    force_authenticate(request, user=user)
    def mock_get_object(self, pk):
        return 1

    monkeypatch.setattr(MovieDetail, "get_object", mock_get_object)

    view = MovieDetail.as_view()
    resp = view(request, pk='1')
    assert resp.status_code == status_code