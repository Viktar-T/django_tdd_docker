# django_tdd_docker


This project is a part of the course: https://testdriven.io/courses/tdd-django/.   
I added **Authorization**, **Middleware**.  
**Tests** were refactord according to my changes. 

### Django app has  several endpoints. 
- "docker compose up -d --build" starts two containers. First one with my Django app. Second with Postres. 
groups, superuser, users are created automatically. See http://127.0.0.1:8009/admin/ (admin, admin_pass).  
- **Endpoints**:   
/api/movies - GET  
/api/movies/:id	- GET  
/api/movies	- POST  
/api/movies/:id	- PUT  
/api/movies/:id	- DELETE    

### Django app has  tests.  
- use "docker compose exec movies pytest" command.
- We are testing models, serializers and views.  
- **Views** are tested in two ways: 
1. In the whole request-response cycle. "Django test client" is used.
1. In isolation. "monkeypatch", "RequestFactory", "force_authenticate()" are used.


### CI with Build and Tests stages was added.  
- First stages (build) passes second stage with tests fails. 
- All tests passes locally. 
- Test stage will be fixed later.

### Project was deployed to Heroku:  
- https://whispering-fortress-58832.herokuapp.com/api/movies/,  
- https://whispering-fortress-58832.herokuapp.com/api/movies/2/, ... 
- to test endpoints in Heroku use http: 
- http GET https://whispering-fortress-58832.herokuapp.com/api/movies/
- http GET https://whispering-fortress-58832.herokuapp.com/api/movies/1/
- http --json POST https://whispering-fortress-58832.herokuapp.com/api/movies/ title=Fargo genre=comedy year=1996
- http --json PUT https://whispering-fortress-58832.herokuapp.com/api/movies/5/ title=Fargo genre=thriller year=1996
- http DELETE https://whispering-fortress-58832.herokuapp.com/api/movies/5/
